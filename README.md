# Shopping list

## Specifications

- User can add items to a shopping list
- Default item quantity is `1`
- User can delete an item
- User can update item quantity (adding one, removing one)
- Item's name is **unique** (tomate !== tomates)
- Adding an existing item increments its quantity
- Setting a quantity at 0 removes the item from the list
