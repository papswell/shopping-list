const storage = new Storage();
const app = new App(storage);

$(function() {
  const $shoppingList = $("#shopping-list");

  // Initial render
  render($shoppingList);

  // Listen for new items
  $("#add-item").on("submit", function(e) {
    e.preventDefault();
    // Récupérer le nom de l'item à ajouter
    const name = $("#item-name").val();
    // Envoi des données à la couche "model"

    try {
      app.addItem(name);
      // update list view
      render($shoppingList);

      // empty the input
      $("#item-name").val("");
    } catch (err) {
      // handle error
      if (err.name === "InvalidParameter") {
        alert("Le nom est obligatoire !");
      } else {
        // re-throw error, to crash the app !!
        throw err;
      }
    }
  });

  // Handler function for all change events
  function handle(e) {
    const $target = $(e.currentTarget);
    const action = $target.data("action");
    const name = $target.data("item");

    if (!action) return;

    switch (action) {
      case "add":
        app.addItem(name);
        break;
      case "remove":
        app.removeItem(name);
        break;
      case "delete":
        app.deleteItem(name);
        break;
      case "toggle":
        app.toggleItem(name);
        break;
    }

    // ou sinon on forge le nom de la méthode dynamiquement
    // app[`${action}Item`](name);

    render($shoppingList);
  }

  $shoppingList.on("click", "button, .item-name", handle);
  $shoppingList.on("change", "input[type='checkbox']", handle);
});

function render($list) {
  // get data from App model
  const items = app.getItemsList();
  const completion = app.getCompletion();

  // display data
  let html = "";
  for (const item of items) {
    html += createLi(item);
  }
  // insert the whole html list
  $list.html(html);

  // Update the completion
  let text;
  let cls;
  if (!items.length) {
    text = "Votre liste de course est vide.";
    cls = "alert-light";
  } else if (completion == 0) {
    text = "Les courses ne sont pas commencées";
    cls = "alert-warning";
  } else if (completion == 100) {
    text = "Les courses sont terminées !!";
    cls = "alert-success";
  } else {
    text = `Courses terminées à ${completion.toFixed(2)}%`;
    cls = "alert-info";
  }

  $list
    .next()
    .attr("class", cls)
    .addClass("alert mt-2")
    .text(text);
}

function createLi(item) {
  return (
    /* html */
    `
  <li 
    class="list-group-item" 
    style="${item.checked ? "opacity: 0.5" : ""}"
  >
    <div class="row">
      <label class="col-1 label-toggle">
        <input 
          type="checkbox" 
          ${item.checked ? "checked" : ""} 
          data-item="${item.name}"
          data-action="toggle">
      </label>
      <div class="col">
        <div 
          class="item-name ${item.checked ? "item-name-checked" : ""}" 
          data-item="${item.name}"
          data-action="toggle"
        >
          ${item.name}
        </div>
      </div>
      <div class="col-auto">
        <div class="btn-toolbar" role="toolbar">
          <div class="btn-group mr-2" role="group">
            ${createButton(item, "-", "remove")}            
            <span class="item-quantity btn btn-light">${item.quantity}</span>
            ${createButton(item, "+", "add")}            
          </div>
          <div class="btn-group" role="group">
            <button class="item-remove btn btn-danger" data-action="delete" data-item="${
              item.name
            }">
            <span>x</span>
            </button>
          </div>
        </div>
      </div>
    </div>
  </li>
  `
  );
}

function createButton(item, label, action) {
  return (
    /* html */
    `
   <button type="button" ${
     item.checked ? "disabled" : ""
   } class="btn btn-light ${
      item.checked ? "btn-disabled" : ""
    }" data-action="${action}" data-item="${item.name}">${label}</button>
   `
  );
}
