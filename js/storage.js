function Storage() {
  this.read = function() {
    const stringData = localStorage.getItem("shoppingList");
    return JSON.parse(stringData);
  };

  this.write = function(data) {
    if (!data || !data.length) {
      localStorage.removeItem("shoppingList");
    } else {
      const stringData = JSON.stringify(data);
      localStorage.setItem("shoppingList", stringData);
    }
  };
}
