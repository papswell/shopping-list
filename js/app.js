function App(storage) {
  /**
   * What needs to be bought
   * @type {object}
   * @example
   * {
   *   milk: 6,
   *   chocolate: 1,
   *   carrots: 12
   * }
   */
  let list = {};

  /**
   * What has alreay been bought
   * @type {string[]}
   * @example ["milk", "chocolate"]
   *
   */
  let checkedItems = [];

  // On remplit la couche modèle avec les données existantes dans le Storage
  const localData = storage.read();
  if (localData && Array.isArray(localData)) {
    for (const data of localData) {
      // on remplit "list"
      list[data.name] = data.quantity;
      // on remplit "checkedItems"
      if (data.checked === true) {
        checkedItems.push(data.name);
      }
    }
  }

  /**
   * Whether the item is already in cart
   * @param {string} name
   */
  function isChecked(name) {
    return checkedItems.includes(name);
  }

  /**
   * Add an item to the cart
   * @param {string} name
   */
  function checkItem(name) {
    checkedItems.push(name);
    // mise a jour du stockage
    storage.write(getItemsList());
  }

  /**
   * Removes an item from the cart
   * @param {string} name
   */
  function uncheckItem(name) {
    checkedItems = checkedItems.filter(n => n !== name);

    storage.write(getItemsList());
    // Raccourci de :

    // checkedItems = checkedItems.filter(function(n) {
    //   if (n === name) return false;
    //   return true;
    // });
  }

  /**
   * Update the cart (add or remove an item)
   * @param {string} name
   */
  function toggleItem(name) {
    validateName(name);
    if (!doesExist(name)) return;

    if (isChecked(name)) {
      uncheckItem(name);
    } else {
      checkItem(name);
    }
  }

  /**
   * Get all items in cart
   * @returns {string[]}
   */
  function getCheckedItems() {
    return checkedItems.slice(); // return a clone, so that the caddie can't be modified from oustide
  }

  /**
   * Whether an item exists or not
   *
   * @param {Item|string} item An instance of Item or a name
   * @returns {bool} true if the item is already in the list
   */
  function doesExist(item) {
    let name = item;
    if (item instanceof Item) {
      name = item.getName();
    }

    return !!list[name]; // cast en boolean
  }

  /**
   * Validates an item name
   * @param {string} name
   * @throws {Error} an error named "InvalidParameter"
   */
  function validateName(name) {
    if (typeof name !== "string" || name === "") {
      const e = new Error(
        `Invalid parameter "${name}" [${typeof name}]. It must be a non empty string`
      );

      e.name = "InvalidParameter";
      throw e;
    }
  }

  /**
   * Add 1 to the quantity of an existing item
   * @param {string} name
   */
  function addItem(name) {
    validateName(name);

    if (doesExist(name)) {
      list[name]++;
    } else {
      list[name] = 1; // default quantity 1
    }

    storage.write(getItemsList());
  }

  /**
   * Remove 1 to the quantity of an existing item
   * @param {string} name
   */
  function removeItem(name) {
    validateName(name);

    if (doesExist(name)) {
      list[name]--;

      if (list[name] === 0) {
        deleteItem(name);
        return; // prevent updating storage twice
      }

      storage.write(getItemsList());
    }
  }

  /**
   * Delete an item from the list
   * @param {string} name
   */
  function deleteItem(name) {
    if (isChecked(name)) {
      uncheckItem(name);
    }

    delete list[name];

    storage.write(getItemsList());
  }

  /**
   * @returns {Array}
   */
  function getItemsList() {
    const array = [];
    for (const key in list) {
      const value = list[key];
      const o = {
        name: key,
        quantity: value,
        checked: isChecked(key)
      };
      array.push(o);
    }

    return array;
  }

  /**
   * Reset the application
   */
  function clear() {
    list = {};
    checkedItems = [];

    storage.write(getItemsList());
  }

  /**
   * @returns {number} the percentage of completion
   */
  function getCompletion() {
    const items = getItemsList();
    return items.length ? (checkedItems.length / items.length) * 100 : 0;
  }

  // ---------------------------------------------------------------------------
  // Public methods
  // ---------------------------------------------------------------------------
  this.addItem = addItem;
  this.removeItem = removeItem;
  this.deleteItem = deleteItem;
  this.toggleItem = toggleItem;
  this.getCheckedItems = getCheckedItems;
  this.getItemsList = getItemsList;
  this.clear = clear;
  this.getCompletion = getCompletion;
}
