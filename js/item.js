/**
 * This is a demo object, to show the purpose of having getter/setter functions.
 *
 * It's not used into the application !
 *
 *
 * @param {string} name
 */
function Item(name) {
  let _name;

  /**
   * Get the item's name
   * @returns {string}
   */
  this.getName = function() {
    return _name;
  };

  /**
   * Set the item's name
   * @param {string} name
   */
  this.setName = function(name) {
    if (typeof name !== "string" || name === "") {
      throw new Error(
        `Invalid parameter "${name}" [${typeof name}]. It must be a non empty string`
      );
    }
    _name = name;
  };

  // Set the name of this instance
  this.setName(name);
}
